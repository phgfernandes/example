package com.phgfernandes.example;

import java.util.HashSet;
import java.util.Set;

public class MainClass {

	public static void main(String[] args) {
		CalculatorFactory cf = new CalculatorFactory();
		Set<String> fieldNames = new HashSet<>();
		fieldNames.add("Age");
		fieldNames.add("CustomerSegment");
		
		for(String name : fieldNames){
			String result = cf.getInstance(name).calculate();
			System.out.println(result);
		}
	}

}

package com.phgfernandes.example;

import java.util.Map;

public class CalculatorFactory {
	Map<String, Calculator> calculatorMap;
	
	{
		calculatorMap.put("Age", new AgeCalculator());
		calculatorMap.put("CustomerSegment", new CustomerSegmentCalculator());
	}
	
	public Calculator getInstance(String fieldName){
		return calculatorMap.get(fieldName);
	}
}
